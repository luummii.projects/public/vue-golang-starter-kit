import Vue from 'vue'
import Vuex from 'vuex'

import __store from './store'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    __store
  }
})

export default store