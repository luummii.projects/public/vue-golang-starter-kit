export default {
  Auth: {
    SignIn: {
      text_1: 'Your Community Awaits',
      text_2: 'SIGN IN',
      text_3: 'Remember Me',
      text_4: 'Forgot Your Password?',
      text_5: 'Don\'t have an <APP> account?',
      text_6: 'Sign Up'
    },
    SignUp: {
      text_1: 'Create Account',
      text_2: 'SIGN UP',
      text_3: 'I agree to the',
      text_4: 'Terms and Conditions',
      text_5: 'CREATE ACCOUNT',
      text_6: 'Have an <APP> account?',
      text_7: 'Sign In'
    }
  }
}