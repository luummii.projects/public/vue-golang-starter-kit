export default {
  SignUp: process.env.NODE_ENV === 'development' ? 'http://localhost:5000/api/v1/signUp' : '/api/v1/signUp'
}