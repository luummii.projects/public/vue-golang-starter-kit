package handlers

import (
	"log"
	"net/http"
	"projects/vue-golang-starter-kit/backend/db"

	"github.com/labstack/echo"
)

// Store -
type Store struct {
	DB *db.Store
}

// SignUp -
func (s *Store) SignUp(c echo.Context) error {
	defer c.Request().Body.Close()

	log.Printf("SignUp")
	return c.String(http.StatusOK, "Horay!")
}
