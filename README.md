# vue-golang-starter-kit (v2.0.0)
vue-golang-starter-kit

### Backend
-Golang
-Echo
-PosrtgeSQL

### Frontend
-VueJS
-Axios
-Sass/Scss
-Semantic

### Migrations
db-migrate up

### Deploy
godep save
.\deploy.sh

### Start
realize start